<?php
require 'vendor/autoload.php';

# Init Logger
//$logger = new Monolog\Logger('name');
//$logger->pushHandler(new Monolog\Handler\StreamHandler('app.log', Monolog\Logger::DEBUG));
//$logger->addDebug("Starting Application...");


use Gaufrette\Filesystem;
use Gaufrette\StreamWrapper;
use Gaufrette\Adapter\Local as LocalAdapter;
//use Gaufrette\Adapter\AwsS3;
use Aws\S3\S3Client;
//use Streamer\Stream;


// Create S3 Connection
$client = S3Client::factory(array(
  'key'    => '',
  'secret' => ''
));

// Register Stream Wrapper for S3 object
$client->registerStreamWrapper();


// Create Local FileSystem
$adapter = new LocalAdapter('/var/tmp');
$filesystem = new Filesystem($adapter);

// Register Stream Wrapper for Local FileSystem
$map = StreamWrapper::getFilesystemMap();
$map->set('video', $filesystem);
StreamWrapper::register('local');

// Initialize Path
//$input = fopen('s3://blog4mob/2013/09/19/girls_0000.jpg', 'r');
//$output = fopen('local://video/girl.jpg', 'w');

$output = fopen('s3://blog4mob/2013/09/19/girls_testme.jpg', 'w');
$input = fopen('local://video/girl.jpg', 'r');

// Verify file pointers exists
if ($input == false || $output == false) {
  throw new Exception("Error");
  //return false;
}

if (!stream_set_chunk_size($input, 8192)) {
  // Failed to configure chunk sizes
}

// pipe dreams!
if (stream_copy_to_stream($input, $output) === 0) { //Returns the total count of bytes copied.
  // Failed to upload
}

// Clean up!
fclose($input);
fclose($output);

?>